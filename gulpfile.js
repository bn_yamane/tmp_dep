
var gulp = require("gulp");
var cleanCss = require("gulp-clean-css");

gulp.task("default", function(done) {
    gulp.src('WebContent/docs_src/**/*')
        .pipe(gulp.dest('WebContent/docs/'));
    gulp.src('WebContent/docs_src/**/*.css')
        .pipe(cleanCss())
        .pipe(gulp.dest('WebContent/docs/'));
    done();
});
