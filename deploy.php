<?php
namespace Deployer;

require 'recipe/common.php';
require 'recipe/npm.php';

inventory('inventory/app.yml');

set('git_tty', true);

task('compile:assets', function () {
    cd('{{release_path}}');
    run('npm run gulp');
    run('rm -rf {{release_path}}/node_modules');
});

task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'npm:install',
    'compile:assets',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
])->desc('Deploy nana-debug');

after('deploy:failed', 'deploy:unlock');
